const form = document.querySelector(".form");
const textareaField = document.querySelector(".textarea");
const addCardItemButton = document.querySelector(".add-card");
const confirmSendCardButton = document.querySelector(".buttons__add");
const cancelSendCardButton = document.querySelector(".buttons__cancel");

let value;
let draggedItem;

function changeTitle() {
  const titleArray = document.querySelectorAll(".title");
  titleArray.forEach((title) => {
    title.addEventListener("click", (e) => {
      e.target.textContent = "";
    });
  });
}

changeTitle();

function removeBoardItem() {
  const boardArray = document.querySelectorAll(".boards__item");
  for (let i = 1; i < boardArray.length; i++) {
    let board = boardArray[i];

    board.addEventListener("dblclick", () => {
      board.remove();
    });
  }
}

removeBoardItem();

function dragAndDrop() {
  const cardList = document.querySelectorAll(".card-list");
  const cardItem = document.querySelectorAll(".card-list__item");

  for (let i = 0; i < cardItem.length; i++) {
    const item = cardItem[i];

    item.addEventListener("dragstart", () => {
      draggedItem = item;
      setTimeout(() => {
        item.style.display = "none";
      }, 0);
    });

    item.addEventListener("dragend", () => {
      draggedItem = null;
      setTimeout(() => {
        item.style.display = "block";
      }, 0);
    });

    item.addEventListener("dblclick", () => {
      item.remove();
    });

    for (let j = 0; j < cardList.length; j++) {
      const item = cardList[j];

      item.addEventListener("dragover", (e) => {
        e.preventDefault();
      });

      item.addEventListener("dragenter", function (e) {
        e.preventDefault();
        this.style.backgroundColor = "rgba(110, 121, 142, .05)";
      });

      item.addEventListener("dragleave", function (e) {
        e.preventDefault();
        this.style.backgroundColor = "rgba(0, 0, 0, 0)";
      });

      item.addEventListener("drop", function (e) {
        e.preventDefault();
        this.style.backgroundColor = "rgba(0, 0, 0, 0)";
        this.append(draggedItem);
      });
    }
  }
}

dragAndDrop();

function addCardItem() {
  addCardItemButton.addEventListener("click", () => {
    form.style.display = "block";
    addCardItemButton.style.display = "none";
    confirmSendCardButton.style.display = "none";
  });
}

addCardItem();

function handleTextareaField() {
  textareaField.addEventListener("input", (e) => {
    value = e.target.value;

    if (value) {
      confirmSendCardButton.style.display = "";
    } else {
      confirmSendCardButton.style.display = "none";
    }
  });
}

handleTextareaField();

function cancelSendCard() {
  const addCardItemButton = document.querySelector(".add-card");

  value = "";
  textareaField.value = "";
  form.style.display = "none";
  addCardItemButton.style.display = "";
}

cancelSendCardButton.addEventListener("click", () => {
  cancelSendCard();
});

function sendCard() {
  const cardList = document.querySelectorAll(".card-list");

  confirmSendCardButton.addEventListener("click", () => {
    const newCard = document.createElement("div");
    newCard.classList.add("card-list__item");
    newCard.draggable = true;
    newCard.textContent = value;
    cardList[0].append(newCard);

    cancelSendCard();
    dragAndDrop();
  });
}

sendCard();

function addBoardItem() {
  const boardList = document.querySelectorAll(".boards");
  const addBoardItemButton = document.querySelector(".add-board");

  addBoardItemButton.addEventListener("click", () => {
    const newBoard = document.createElement("div");
    newBoard.classList.add("boards__item");
    newBoard.innerHTML = `
    <span contenteditable="true" class="title">Enter board name</span>
    <div class="card-list"></div>
  `;
    boardList[0].append(newBoard);

    dragAndDrop();
    changeTitle();
    removeBoardItem();
  });
}

addBoardItem();
